Contact No Subject

Extract the module to your modules folder and then enable from 
admin/build/modules

Once enabled the module will hide the subject field for all site-wide contact 
forms.

To re-add the subject field simply disable the module. 
